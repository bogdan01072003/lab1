import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class matchesRegex extends TypeSafeMatcher<String> {

    private final String regex;

    public matchesRegex(String regex) {
        this.regex = regex;
    }

    @Override
    protected boolean matchesSafely(String item) {
        return item.matches(regex);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("matches regex \"" + regex + "\"");
    }

    @Override
    protected void describeMismatchSafely(String item, Description mismatchDescription) {
        mismatchDescription.appendValue(item).appendText(" does not match regex \"" + regex + "\"");
    }
}
