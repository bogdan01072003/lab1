import org.example.Main;

import java.util.*;

import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.Parameterized;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.*;

import static org.junit.Assert.*;
@RunWith(JUnit4.class)
public class MainTest {
    private static String path;
    private static File file;
    private static String input = "abc def ghi jklm nop qrs tuv wxyz";
    @BeforeClass
    public static void setup() throws IOException {

        path = "C:\\Users\\bogda\\lab\\test.txt";
        file = new File(path);
        BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
        writer.write(input);
        writer.close();

    }
    @AfterClass
    public static void delete()
    {
        file.deleteOnExit();
    }

    @Test
    public void testSplitWordsWithValidInput() {
        String text = "This is a valid input text.";
        String[] expected = {"This", "is", "a", "valid", "input", "text"};
        assertArrayEquals(expected, Main.splitWords(text));
    }

    @Test
    public void testSplitWordsWithInvalidInput() {
        String text = "";
        assertNull(Main.splitWords(text));
    }

    @Test
    public void testResultWithValidInput() {
        String[] words = {"hello", "world", "java", "test", "super", "programming"};
        ArrayList<String> expected = new ArrayList<>(Arrays.asList("world", "super"));
        assertEquals(expected, Main.result(words));
    }

    @Test
    public void testResultWithInvalidInput() {
        String[] words = {"a", "aa", "aaa", "aaaa"};
        ArrayList<String> expected = new ArrayList<>();
        expected.add("a");
        assertEquals(expected, Main.result(words));
    }

    @Test(expected = NullPointerException.class)
    public void testReadFromFileWithNullArgument() {
        String path = null;
        String result = Main.readFromFile(path);
        assertNull(result);
    }

    @Test
    public void testResultWithHamcrest() {
        String[] words = {"aple", "banana", "cherry", "aple"};
        ArrayList<String> expected = new ArrayList<>(Arrays.asList("aple"));
        ArrayList<String> actual = Main.result(words);
        assertThat(actual, Matchers.containsInAnyOrder(expected.toArray()));
    }
    @Test
    public void testReadFromFile_onlyLetters() {
        String expected = "abc def ghi jklm nop qrs tuv wxyz";
        String actual = Main.readFromFile(path);
        assertThat(actual, is(new matchesRegex("^[a-zA-Z\\s]+$")));
    }
    @Test
    public void testReadFromFile_fileExists() {
        String actual = Main.readFromFile(path);
        assertThat(actual, equalTo(input));
    }

    @ParameterizedTest
    @CsvSource({
            "!@#$%^&*,",
            "This is a sentence, This is a sentence",
            "Supercalifragilisticexpialidocious, Supercalifragilisticexpialido",
            "Pneumonoultramicroscopicsilicovolcanoconiosis,Pneumonoultramicroscopicsilic",
            "   Multiple     whitespaces     , Multiple whitespaces",

    })
    void testSplitWords(String input, String expectedOutput) {
        assertArrayEquals(expectedOutput != null ? expectedOutput.split(" ") : null, Main.splitWords(input));
    }

}